T8Converter
===========

MediaWiki XML to HTML Converter for a specific project - an internal person database, which partially goes external.

Versions used:

MediaWiki 1.15.1 (also tested with 1.22.6)

Joomla 3.1.1

This project is released Copyright (C) 2013 Pascal Lehner under the GPL V3. For full license text see COPYING.


1. Concept
----------
This program takes MediaWiki export XML and creates a *.html file for each <page> container in the XML file. I only care about the containers I need for this.
The container <text> contains all the information on the wiki page itself. I handle internal files to other persons, external links, picture links, and tags.
Furthermore, I create an index.html file in the root export folder with links to all MediaWiki pages, sorted by last MediaWiki title container.

The code surely can use some optimisation. However, for the intended use once every few months speed is not crucial.
Performance is quite nice anyway, on a MacBook Air late 2012 edition it took about 5-6 seconds to generate 280 html files from one XML file.


2. Building
-----------
I used lcVCS and mergJSON to create a GIT style repository for my livecode file. This allows me to share code with you all and you can export it. However, I also uploaded the afaConverter.livecode file if you just want to have a quick look at the whole thing.
https://github.com/montegoulding/lcVCS


3. Installation
---------------
Download the folder for your OS from /builds/ and copy them to a location you like. Then run T8Converter.app or T8Converter.exe


4. Details
----------
See documentation or source code for further details.

5. Contribution
---------------
Feel free to contribute to this project to make it more widely usable. Even though the UI is in german, all source code comments are in english.



6. Contact me
--------------
Please contact me using GitHub's toolset or just write to tate83@gmail.com - or tweet @tate83